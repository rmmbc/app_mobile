import { Component } from '@angular/core';
import { FilterPipe } from '../service/filter.pipe';

@Component({
  selector: 'app-question',
  templateUrl: 'question.page.html',
  styleUrls: ['question.page.scss']
})


export class QuestionPage {

  searchText: string;
  filtred:any[];
The_categories: any[] = [
  {
  "id": 3,
  "nomCategorie": "IT",
  "faq": 
  [{
      "id": 6,
      "question": "Qu'est ce qu'il faut utiliser dans le projet de FAQ coté mobile ?",
      "reponse": "Ionic et Angular "
    },
    {
      "id": 7,
        "question": "Combien d'équipe y a t-il dans le projet FAQ ?",
        "reponse": "Il y a 3 équipes : Deux équipes de 4 personnes et une équipe de 5 "
    },
    {
      "id": 8,
      "question": "Qu'est ce qu'il faut utiliser dans le projet de FAQ coté mobile ?",
      "reponse": "Ionic et Angular "
    }]
    },
    {
      "id": 4,
      "nomCategorie": "Mecanique",
      "faq": 
      [{
          "id": 1,
          "question": " Combien de moteurs peut on avoir dans une voiture ?",
          "reponse": "Normalement 1 seul "
        },
        {
          "id": 2,
          "question": " C'est quoi les histoire de cheuvaux la 2 cheveaux 4 chevaux ?",
          "reponse": "Je ne sais pas ce que les cheveaux viennent faire dans une histoire de mécanique "
        },
        {
          "id": 3,
          "question": "Qu'est ce qu'il faut utiliser dans le projet de FAQ coté mobile ?",
          "reponse": "Ionic et Angular "
        }]
        },

        {
          "id": 5,
          "nomCategorie": "Medecine",
          "faq": 
          [{
              "id": 4,
              "question": "Qu'est ce que la leucémie ?",
              "reponse": "C'est une maladie en rapport avec le manque de globulue blanc je pense "
            },
            {
              "id": 5,
              "question": "Comment soigne t-on la leucémie ?",
              "reponse": "Je ne sais pas vraiment ... Est ce que c'est soignable ? "
            }]
          }
  ]
  all:any[]=this.The_categories;

  constructor() {
   
  }


  segmenteChanged(ev: any) {
    if (ev.detail.value=="All") {
      this.The_categories=this.all;      
    } else {
      this.The_categories=this.all.filter(elmnt=> elmnt.nomCategorie==ev.detail.value);
    }
  }
 
}

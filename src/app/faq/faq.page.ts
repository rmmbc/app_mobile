import { Component } from '@angular/core';
import {MatExpansionModule} from '@angular/material/expansion';
import { Faq } from '../Models/faq';
import { FaqServicesService } from '../service/faq-services.service';
import { UtilsService } from '../utils.service';
import { Categories } from '../Models/categories';
import { CategorieServicesService } from '../service/categorie-services.service';


@Component({
  selector: 'app-faq',
  templateUrl: 'faq.page.html',
  styleUrls: ['faq.page.scss']
})
export class FaqPage {

  categories = [
    [1, 2, 3, 4, 5],
    [1, 2, 3, 4],
    [1, 2, 3] 
  ]; 

  searchText: string;


The_categories: any[] = [
  {
  "id": 3,
  "nomCategorie": "IT",
  "faq": 
  [{
      "id": 6,
      "question": "Qu'est ce qu'il faut utiliser dans le projet de FAQ coté mobile ?",
      "reponse": "Ionic et Angular "
    },
    {
      "id": 7,
        "question": "Combien d'équipe y a t-il dans le projet FAQ ?",
        "reponse": "Il y a 3 équipes : Deux équipes de 4 personnes et une équipe de 5 "
    },
    {
      "id": 8,
      "question": "Qu'est ce qu'il faut utiliser dans le projet de FAQ coté mobile ?",
      "reponse": "Ionic et Angular "
    }]
    },
    {
      "id": 4,
      "nomCategorie": "Mecanique",
      "faq": 
      [{
          "id": 1,
          "question": " Combien de moteurs peut on avoir dans une voiture ?",
          "reponse": "Normalement 1 seul "
        },
        {
          "id": 2,
          "question": " C'est quoi les histoire de cheuvaux la 2 cheveaux 4 chevaux ?",
          "reponse": "Je ne sais pas ce que les cheveaux viennent faire dans une histoire de mécanique "
        },
        {
          "id": 3,
          "question": "Qu'est ce qu'il faut utiliser dans le projet de FAQ coté mobile ?",
          "reponse": "Ionic et Angular "
        }]
        },

        {
          "id": 5,
          "nomCategorie": "Medecine",
          "faq": 
          [{
              "id": 4,
              "question": "Qu'est ce que la leucémie ?",
              "reponse": "C'est une maladie en rapport avec le manque de globulue blanc je pense "
            },
            {
              "id": 5,
              "question": "Comment soigne t-on la leucémie ?",
              "reponse": "Je ne sais pas vraiment ... Est ce que c'est soignable ? "
            }]
          }
  ]

  faqs: Faq[];
  the_categories: Categories[];

  constructor(private serviceFaq: FaqServicesService, private utils: UtilsService, private serviceCategorie: CategorieServicesService) {
    this.getFaqs(); 
    this.getCategories();
   } 
  
  ngOnInit(){
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function () {
        /* Toggle between adding and removing the "active" class,
        to highlight the button that controls the panel */
        this.classList.toggle("active");

        /* Toggle between hiding and showing the active panel */
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
          panel.style.display = "none";
        } else {
          panel.style.display = "block";
        }
      });
    }  
  }

  step = 0;

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  getFaqs():void 
  {
    this.serviceFaq.getFaqs().subscribe(faqs =>{
        this.faqs = faqs;
        console.log(faqs);
    }, 
    error=>
    { 
      console.log('error', error);
      this.utils.presentToast('Erreur survenue','danger');

    });
  }

  getCategories():void 
  {
    this.serviceCategorie.getCategories().subscribe(cat =>{
        this.the_categories = cat;
        console.log(cat); 

    }, 
    error=>
    { 
      console.log('error', error);
      this.utils.presentToast('Erreur survenue','danger');

    });
  }

}

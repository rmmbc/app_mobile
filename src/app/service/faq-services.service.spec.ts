import { TestBed } from '@angular/core/testing';

import { FaqServicesService } from './faq-services.service';

describe('FaqServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FaqServicesService = TestBed.get(FaqServicesService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Faq } from '../Models/faq';
import {URL} from '../../environments/environment';
import { identifierModuleUrl } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})

export class FaqServicesService {
  
  constructor(private http: HttpClient) { }
   httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Access-Control-Allow-Origin':'*'
    })
  };

  getFaqs( ): Observable<Faq[]>{
    return this.http.get<Faq []>(URL+'/faqs/',this.httpOptions).pipe();
  }

  getFaq( id : Number) : Observable<Faq>
  {
    return this.http.get<Faq>(URL+'/faqs/'+id).pipe();
  } 

  postFaq( faq : Faq) : Observable<Faq>
  {
    return this.http.post<Faq>(URL+'/faqs',faq).pipe();
  }

  deleteFaq(id: Number) : Observable<Faq>
  {
    return this.http.delete<Faq>(URL+'/faqs/'+id).pipe();
  }
  updateFaq( faq: Faq)
  {
    return this.http.put<Faq>(URL+'/faqs/'+faq.id,faq).pipe();
  }

}

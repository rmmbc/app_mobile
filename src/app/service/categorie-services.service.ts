import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Categories } from '../Models/categories';
import {URL} from '../../environments/environment';
import { identifierModuleUrl } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class CategorieServicesService {

  constructor(private http: HttpClient) { }
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Access-Control-Allow-Origin':'*'
    })
  };

  getCategories( ): Observable<Categories[]>{
    return this.http.get<Categories []>(URL+'/categorie',this.httpOptions).pipe();

  }

  getCategorie( id : Number) : Observable<Categories>
  {
    return this.http.get<Categories>(URL+'/categorie/'+id).pipe();
  }

  postCategorie( categorie : Categories) : Observable<Categories>
  {
    return this.http.post<Categories>(URL+'/categorie',categorie).pipe();
  }

  deleteCategorie(id: Number) : Observable<Categories>
  {
    return this.http.delete<Categories>(URL+'/categorie/'+id).pipe();
  }
  updateCategorie( categorie: Categories)
  {
    return this.http.put<Categories>(URL+'/categorie/'+categorie.id,categorie).pipe();
  }

}

export class Faq {
    id: number;
    question: String;
    reponse: String;
    date: Date;
    categorie: String;
}

import { Faq } from './faq';

export class Categories {
    id : Number;
    nomCategorie: String;
    faq: Faq[];
}

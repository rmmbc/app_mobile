import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HighContrastModeDetector, FocusMonitor } from '@angular/cdk/a11y';
import { Platform } from '@angular/cdk/platform';
import { UniqueSelectionDispatcher } from '@angular/cdk/collections';
import { MatIconRegistry } from '@angular/material/icon';
import { AutofillMonitor } from '@angular/cdk/text-field';
import { ContentObserver, MutationObserverFactory } from '@angular/cdk/observers';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, HttpClientModule, IonicModule.forRoot(), AppRoutingModule,BrowserAnimationsModule],
  providers: [
    StatusBar,
    FocusMonitor,
    MatIconRegistry,
    UniqueSelectionDispatcher,
    HighContrastModeDetector,
    Platform,
    AutofillMonitor,
    ContentObserver,
    MutationObserverFactory,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
